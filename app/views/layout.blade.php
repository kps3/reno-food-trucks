<!doctype HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{{URL::to('/')}}}/lib/foundation/css/foundation.css" type="text/css" />
    <!-- SITE STYLES -->
    <link rel="stylesheet" href="{{{URL::to('/')}}}/css/global.css" type="text/css" />
    <link rel="stylesheet" href="{{{URL::to('/')}}}/css/subpage.css" type="text/css" />
    <script src="{{{URL::to('/')}}}/js/lib/jquery/jquery-1.10.2.min.js"></script>
    <script src="{{{URL::to('/')}}}/js/Page.js"></script>
    <script src="{{{URL::to('/')}}}/js/Page.js"></script>
    <script src="{{{URL::to('/')}}}/js/globals.js"></script>

    <!-- FONTS -->
    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/616688/642882/css/fonts.css" />

    <meta name="viewport" content="width=device-width, user-scalable=no">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-44655106-1', 'wherethefoodat.com');
        ga('send', 'pageview');

    </script>
    <!--[if lt IE 9]>
    <script src="{{{URL::to('/')}}}/lib/html5shiv/html5shiv.js"></script>
    <![endif]-->
</head>
<body class="secondary">
    <div id="message-error"></div>
    <div id="message-success"></div>
        @yield('content')
        @yield('scripts')
    </body>
</html>
