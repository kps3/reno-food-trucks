@extends('layout')
@section('content')
<form method="post" action="{{{route('loginPost')}}}" id="login-form">
  <div class="row">
    <div class="columns large-12 centered"><div class="logo"><a href="/">Where the food at?</a></div></div>
    <div class="columns large-12 centered"><input type="text" placeholder="Username" name="username" /></div>
    <div class="columns large-12 centered"><input type="password" placeholder="Password" name="password" /></div>
    <div class="columns large-12 centered"><a href="{{{route('register')}}}" class="button transparent" id="sign-up">&laquo; Request an account</a></a><a href="javascript:void(0)" onclick="$('#login-form').submit()" class="button">Login</a></div>
  </div>
</form>


@stop