@extends('truck')
@section('content')

<form method="post" id="truck-profile" autocomplete="false">
  <div>
    <label>Name:</label> <input type="text" name="name" required value="{{{$api['name']}}}">
  </div>
  <div>
    <label>Description:</label> <textarea name="description" required>{{{$db->description}}}</textarea>
  </div>
  <div>
    <label>Phone:</label> <input type="tel" name="phone" value="{{{@$api['social']['phone']}}}">
  </div>
  <div>
    <label>URL:</label> <input type="url" name="url" value="{{{@$api['social']['url']}}}">
  </div>
  <div>
    <label>URL To Image:</label> <input type="url" name="image" value="{{{@$db->image}}}">
  </div>
  <div>
    <label>Email:</label> <input type="email" name="email" required value="{{{$db->email}}}">
  </div>
  <div>
    <label>Password New:</label>
    <input type="password" name="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$">
  </div>
  <div>
    <label>Re-enter New Password:</label>
    <input type="password" name="password2" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$">
  </div>
  <div>
    <label>Food Types:</label>
    <select name="tags[]" size="5" multiple>
      <option></option>
      @foreach ($foodTypes as $id=>$type)
      <option
      {{in_array($type, $api['tags'])?'selected':''}}>{{{$type}}}</option>
      @endforeach
    </select>
  </div>

  <div id="spinner" style="display:none"></div>
  <div id="button">
    <input type="submit" value="Update">
  </div>
</form>

@stop

@section('scripts')
<script src="{{{URL::to('/')}}}/js/Pages/Truck/Profile.js"></script>
<script src="{{{URL::to('/')}}}/js/lib/spin/spin.min.js"></script>
@stop