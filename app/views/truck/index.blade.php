@extends('truck')
@section('content')
<a href="/truck/profile">Edit Profile</a>
<div id="message-error"></div>
<a style="display: block; width: 100%; text-align: center; float: left; clear: both; display:none" class="button show-on-location control-button" data-control="start">Start Truckin'</a><br /><br />
<a style="display: block; width: 100%; text-align: center; float: left; clear: both; display:none" class="button transparent show-on-location control-button" data-control="stop">Stop Truckin'</a>
<br clear="all" /><br /><br /><br /><br /><br /><br /><br />
<div>Status: <span id="status">{{{$active}}}</span></div>
@stop

@section('scripts')
<script src="{{{URL::to('/')}}}/js/Pages/Truck/Index.js"></script>
@stop