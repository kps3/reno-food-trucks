@extends('layout')
@section('content')
<div class="row">
    <div class="columns large-12 centered"><div class="logo"><a href="/">Where the food at?</a></div></div>
    <div class="columns large-6">
        <h1>Request a food truck account</h1>
        <p>Every reno foodie wants your eats... they just don't know how to find you... and they are certainly not going to follow 20 trucks on twitter or facebook. So signup, start using the app and we can promise the people will come. Hey, at a minimum, the five of us that built this app will come!</p>
        <p>Creating an account is simple... just create an account and start using the app. We built in some smarts so that if you forget to turn off the app and you head home, we will turn your status to not active.</p></div>
    <div class="columns large-6 signup">
    <form id="register-form" autocomplete="false">
        <div class="columns large-12"><input type="text" name="name" required placeholder="Truck Name" /></div>
        <div class="columns large-12"><textarea name="description" required placeholder="Description"></textarea></div>
        <div class="columns large-12"><input type="tel" name="phone" placeholder="Phone" /></div>
        <div class="columns large-12"><input type="email" name="email" required placeholder="Email" /></div>
        <div class="columns large-12"><input type="email" name="email2" required placeholder="Re-enter Email" /></div>
        <div class="columns large-12"><input type="text" name="username"  placeholder="Username" required pattern="[a-zA-Z0-9]+" /></div>
        <div class="columns large-12"><input type="password" name="password"  placeholder="Password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" /></div>
        <div class="columns large-12"><input type="password" name="password2"  placeholder="Re-Enter Password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" /></div>
        <div class="columns large-12"><label>Food Type:</label>
          <select name="tags[]" size="5" multiple>
            <option></option>
            @foreach ($foodTypes as $id=>$type)
            <option id="{{{$id}}}">{{{$type}}}</option>
            @endforeach
          </select>
        </div>
        <div id="spinner" style="display:none"></div>
        <div class="ctas columns large-12"><a href="/login" class="button transparent" id="sign-up">&laquo; I have an account</a></a><a id="request" href="javascript:void(0)" onclick="$('#register-form').submit()" class="button">Request</a></div>
    </form>
    </div>
</div>

@stop

@section('scripts')
<script src="{{{URL::to('/')}}}/js/Pages/Register.js"></script>
<script src="{{{URL::to('/')}}}/js/lib/spin/spin.min.js"></script>
@stop

