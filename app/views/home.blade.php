<!doctype HTML>
<html>
<head>
  <!-- SITE STYLES -->
  <link rel="stylesheet" href="{{{URL::to('/')}}}/css/global.css" type="text/css" />
  <link rel="stylesheet" href="{{{URL::to('/')}}}/css/homepage.css" type="text/css" />
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
  <script src="{{{URL::to('/')}}}/js/lib/jquery/jquery-1.10.2.min.js"></script>
  <script src="{{{URL::to('/')}}}/js/globals.js"></script>
  <script src="{{{URL::to('/')}}}/js/Page.js"></script>
  <script src="{{{URL::to('/')}}}/js/Pages/Index.js"></script>
  <script src="{{{URL::to('/')}}}/lib/jquery/plugins/sort/jquery.tinysort.min.js"></script>
  <!-- FONTS -->
  <link rel="stylesheet" type="text/css" href="//cloud.typography.com/616688/642882/css/fonts.css" />

  <meta name="viewport" content="width=device-width, user-scalable=no">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-44655106-1', 'wherethefoodat.com');
        ga('send', 'pageview');

    </script>
  <!--[if lt IE 9]>
  <script src="{{{URL::to('/')}}}/lib/html5shiv/html5shiv.js"></script>
  <![endif]-->

</head>
<body class="home">
<div class="container animated">
  <header id="header">
    <a href="#" class="back mobile animated">Back</a>
    <a class="button transparent" id="register" href="{{{URL::route('register')}}}">Register Your Truck</a>
    <div class="logo"><a href="/">Where the food at?</a></div>
    <a class="button" id="login" href="{{{URL::route('login')}}}">Login</a>
    <a id="menu" href="#">About</a>
    <a href="{{{URL::route('register')}}}" class="addTruck mobile"></a>
  </header>
  <div class="sidebar animated">
    <ul id="trucks">
    </ul>
    <div class="copyright animated">
      <p>Made in Reno&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#">API Docs</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a id="about" href="#">About</a></a></p>
    </div>
  </div>
  <div class="truck-content animated">
    <div class="close">x</div>
    <img src="" alt="" />
    <div class="content">
      <h1 class="name"></h1>
      <div class="description">
      </div>
      <div class="phone"></div>
      <h1>Want an update on the go?</h1>
      <div class="sms">Text message "where is <span class="slug"></span>" to 1.702.625.8664 for instant updates</div>
    </div>
  </div>
  <div class="map">
    <div id="map-canvas"></div>
  </div>
</div>
<nav id="nav" class="animated">
    <h1>About</h1>
    <p>Food is good... food that comes off a truck is more good. This is a fact, a universal truth and the reason that every food truck should broadcast proudly on our site and in our apps.</p>
    <p>Where the food at was created at Hack4Reno 2013 by:</p>
    <div class="person">
        <div class="headshot"><img src="{{{URL::to('/')}}}/img/kev-80x80.png" /></div>
        <h2>Kevin Jones<span>Design + Front-End</span></h2>
        <p>Art Director at KPS3. Co-founder of Reno Wired. Previous designer at Cloudsnap. Battle-scarred agency designer. <a href="http://kps3.com/company/crew/kevin-jones">Get more.</a></p>
    </div>
    <div class="person">
        <div class="headshot"><img src="{{{URL::to('/')}}}/img/andy-80x80.png" /></div>
        <h2>Andy Muth<span>Back-End + DevOps</span></h2>
        <p>Code monkey and creator of Geek Fire Salsa... that about sums it up. <a href="http://kps3.com/company/crew/andy-muth">Get more.</a></p>
    </div>
    <div class="person">
        <div class="headshot"><img src="{{{URL::to('/')}}}/img/chris-80x80.png" /></div>
        <h2>Christopher Baker<span>API + Back-End</span></h2>
        <p>Christopher is master of developing HTML5 slot machines, enjoys a little node.js and considers Wordpress a bad word. <a href="http://www.hmudesign.com/">Get more.</a></p>
    </div>
    <div class="person">
        <div class="headshot"><img src="{{{URL::to('/')}}}/img/jonathan-80x80.png" /></div>
        <h2>Jonathan Rutheiser<span>Front-End + PhoneGap</span></h2>
        <p>Jonathan has been programming web sites and applications for 7+ years. He has come a long way since Geocities. <a href="http://thelostbit.com/">Get More.</a></p>
    </div>
    <div class="person">
        <div class="headshot"><img src="{{{URL::to('/')}}}/img/rob-80x80.png" /></div>
        <h2>Rob Gaedtke<span>PM + Content + Direction</span></h2>
        <p>Partner/Creative Director @KPS3, husband to @shaynagaedtke & father of two. If it makes my stomach drop or my mind rack, I'm in. <a href="http://kps3.com/company/crew/rob-gaedtke">Get more.</a></p>
    </div>
    <p class="youcantsueus">We made this site for you and we did it on our own time and with our own tools. Use of this website and application are completely at your own risk and we assume no responsibility for anything that happens. We tried our best to make the site bug free and hacker free, but nothing is perfect and certainly not anything that was built in 24 hours. Also, please remember to turn off the tracker when your truck is done serving, you don’t want people knocking on your door for a sandwich at 3am.</p>
</nav>

</body>
</html>

