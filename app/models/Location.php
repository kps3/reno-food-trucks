<?php
  class Location {
    public $Latitude;
    public $Longitude;

    public function __construct($long, $lat = null) {
      if ($long && !$lat) {
        if (strpos($long, ',')!==false) {
          list($long, $lat) = explode(',', $long);
        }
        else {
          throw new Exception('Invalid Location');
        }
      }
      $this->Longitude = $long;
      $this->Latitude = $lat;
    }

    public function __toString() {
      return "{$this->Longitude},{$this->Latitude}";
    }
  }