<?php
  class API {

    private $_transport;
    private $_headers;
    private $_baseUrl = 'http://wherethefoodatapi.jit.su/';

    public function Find($location = null, $slug = null, $tags = null) {
      $this->initTransport();
      $data = [];
      if ($location instanceof Location || is_string($location)) {
        $data['location'] = (string)$location;
      }
      if (is_array($tags)) {
        $data['tags'] = implode(',', $tags);
      }
      if ($slug) {
        $data['slug'] = $slug;
      }
      $this->buildGet('find', $data);
      return $this->executeRequest();
    }

    public function Update($slug, $name = null, $tags = null, $location = null, $social = null, $active = true, $approved = true) {
      $this->initTransport();
      $data = [];
      if ($slug) {
        $data['slug'] = $slug;
      }
      if ($social && is_array($social)) {
        $data['social'] = http_build_query($social);
      }
      if ($name) {
        $data['name'] = $name;
      }
      if (is_array($tags)) {
        $data['tags'] = implode(',', $tags);
      }
      if ($location instanceof Location || is_string($location)) {
        $data['location'] = (string)$location;
      }
      if (!$active) {
        $data['active'] = '0';
      }
      if ($approved) {
        $data['approved'] = '1';
      }
      elseif ($approved === false) {
        $data['approved'] = '0';
      }
      $this->buildPost('update', $data);
      return $this->executeRequest();
    }

    public function Relocate($slug, $location) {
      return $this->Update($slug, null, null, $location, null, true, null);
    }

    /**
     * Gets the the curl resource, with the base options set, that will be used to make the next API call.  To make
     * a complete API call, the _initTransport method, _build* method and _executeRequest method must be called
     * in that order.
     */
    protected function initTransport() {
      $ch = curl_init();
      curl_setopt_array(
        $ch, [
        CURLOPT_RETURNTRANSFER => true,
      ]);
      $this->_headers = array();
      unset($this->_transport);
      $this->_transport = $ch;
    }

    /**
     * Builds the request for a GET method.
     * @param $method string the API method to call.
     * @param mixed $data array of key=>value post values or a string.
     */
    protected function buildGet($method, $data) {
      $this->_headers[] = 'Content-type: application/json';
      $this->_headers[] = 'Accept: application/json';
      $url = $this->_baseUrl . $method . '?' . http_build_query($data);
      curl_setopt($this->_transport, CURLOPT_POST, false);
      curl_setopt($this->_transport, CURLOPT_URL, $url);
    }

    /**
     * Builds the request for a POST method.
     * @param $method string the API method to call.
     * @param mixed $data array of key=>value post values or a string.
     * @param $contentType string
     */
    protected function buildPost($method, $data) {
      $url = $this->_baseUrl . $method;
      $this->_headers[] = 'Accept: application/json';
      curl_setopt($this->_transport, CURLOPT_POST, true);
      curl_setopt($this->_transport, CURLOPT_POSTFIELDS, http_build_query($data));
      curl_setopt($this->_transport, CURLOPT_URL, $url);
    }

    protected function executeRequest() {
      curl_setopt($this->_transport, CURLOPT_HTTPHEADER, $this->_headers);
      $result = curl_exec($this->_transport);
      $code = curl_getinfo($this->_transport, CURLINFO_HTTP_CODE);
      if (curl_errno($this->_transport) !== 0) {
        throw new Exception(curl_error($this->_transport), curl_errno($this->_transport));
      }
      if ($code != 200) {
        throw new Exception('API Request Returned ' . $code);
      }
      if (!$result) {
        throw new Exception('Empty API response.');
      }

      $json = json_decode($result, true);
      if ($json === null) {
        throw new Exception('Could not decode API response.');
      }

      return $json;
    }
  }


