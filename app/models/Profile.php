<?php
class Profile extends Purekid\Mongodm\Model implements \Illuminate\Auth\UserInterface {

  static $collection = 'profiles';
  public $name;
  public $tags;
  public $location;
  public $social;

  public function __construct($data = array()) {
    parent::__construct($data);
    Purekid\Mongodm\MongoDB::instance()->ensure_index($this->collectionName(), 'username', array('unique' => true));
  }

  public function register() {
    $api = new API();
    if (Profile::one(['username' => $this->username])) {
      throw new Exception('Username already exists');
    }
    $api->Update($this->username, $this->name, $this->tags, null, $this->social, false, false);
    $data = [
      'url' => URL::to('/'),
      'name' => $this->name
    ];

    mail('andy@kps3.com','New Truck', print_r($this, true));

    $this->save();
  }

  public function UpdateProfile() {
    $api = new API();
    $api->Update($this->username, $this->name, $this->tags, null, $this->social, true);
    $this->save();
  }

  public function SetPassword($password) {
    $this->password = Hash::make($password);
  }

  public function getAuthIdentifier() {
    return $this->username;
  }

  public function getAuthPassword() {
    return $this->password;
  }

  protected static $attrs = array(
    'username'=>array('type'=>'string'),
    'password'=>array('type'=>'string')
  );
}