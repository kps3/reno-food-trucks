<?php

  Route::filter(
    'mobile.auth', function () {
    if (Input::get('token')) {
      $parts = explode('.', Input::get('token'));
      $id = @$parts[0];
      $hash = @$parts[1];
      $mongoID = new MongoID($id);
      $profile = Profile::id($mongoID);
      if (!$profile) {
        throw new Exception('Invalid Username and/or Password');
      }
      if (md5($profile->username . $profile->password) == $hash) {
        BaseMobileController::SetProfile($profile);
        return;
      }
      else {
        throw new Exception('Invalid Token');
      }
    }
    else {
      throw new Exception('Token is required.');
    }
  });

  Route::filter(
    'auth', function () {
    if (!Session::get('username')) {
      return \Redirect::to(\URL::to('/'));
    }
  });

  Route::get('/register', ['as' => 'register', 'uses' => 'RegisterController@Index']);
  Route::get('/registered', ['as' => 'registered', 'uses' => 'RegisterController@Registered']);
  Route::post('/register', ['as' => 'registerPost', 'uses' => 'RegisterController@Register']);

  Route::get('/', ['as' => 'home', 'uses' => 'HomeController@Index']);
  Route::get('/login', ['as' => 'login', 'uses' => 'LoginController@LoginPage']);
  Route::post('/login', ['as' => 'loginPost', 'uses' => 'LoginController@Login']);

  Route::get('/truck', ['as' => 'truck', 'uses' => 'TruckController@Index']);
  Route::get('/truck/logout', ['as' => 'truckLogout', 'uses' => 'TruckController@Logout']);
  Route::get('/truck/profile', ['as' => 'truckProfile', 'uses' => 'TruckController@Profile']);
  Route::post('/truck/profile', ['as' => 'truckProfileUpdate', 'uses' => 'TruckController@Update']);
  Route::post('/truck/button', ['as' => 'truckButton', 'uses' => 'TruckController@Button']);

  Route::get('/wherethefoodat', ['as' => 'wherethefoodat', 'uses' => 'HomeController@Trucks']);
  Route::get('/wherethefoodat/{slug}', ['as' => 'wherethefoodatTruck', 'uses' => 'HomeController@Truck']);

  Route::post('/mobile/auth', ['as' => 'mobileAuth', 'uses' => 'MobileAuthController@Auth']);
  Route::post('/mobile/update', ['as' => 'mobileUpdate', 'uses' => 'TruckMobileController@Update']);

  Route::post('/mobile/tropo', ['as' => 'tropo', 'uses' => 'TropoController@Text']);
  Route::get('/mobile/tropo', ['as' => 'tropoGet', 'uses' => 'TropoController@Text']);