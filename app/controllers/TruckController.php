<?php

  class TruckController extends BaseTruckController {

    public function __construct() {
      parent::__construct();
      $this->_api = new API();
    }

    public function Index() {
      $data = $this->_getProfileFromAPI();
      $active = (@$data['active']) ? 'Active' : 'Inactive';
      return View::make('truck.index')->with('active', $active);
    }

    public function Logout() {
      session_destroy();
      return Redirect::To('/');
    }

    private function _getProfileFromAPI() {
      return head($this->_api->Find(null, Session::get('username')));
    }

    public function Profile() {
      $data = $this->_getProfileFromAPI();
      return View::make('truck.profile')->with('api', $data)->with('db', $this->GetProfile())->with('foodTypes', Config::get('hardcoded.food-types'));
    }

    public function Button() {
      $input = Input::only('start', 'lat', 'long');
      $api = new API();
      if ($input['start'] !== 'false') { // Turn on
        $location = new Location($input['long'], $input['lat']);
        $result = $api->Relocate(Session::get('username'), $location);
      } else {
        $result = $api->Update(Session::get('username'), null, null, null, null, false, null);
      }
      return Response::json(
        [
          'success' => true
        ]);
    }

    public function Update() {
      $input = Input::only('image', 'username', 'password', 'password2', 'tags', 'description', 'name', 'email', 'email2', 'url', 'phone');
      $user = $this->GetProfile();
      if ($input['password']) {
        if ($input['password'] != $input['password2']) {
          throw new Exception('Passwords do not match.');
        }
        $user->SetPassword($input['password']);
      }
      $user->name = $input['name'];
      $user->tags = $input['tags'];
      $user->description = $input['description'];
      $user->email = $input['email'];
      $user->social = [
        'url'   => $input['url'],
        'phone' => $input['phone']
      ];
      $user->image = $input['image'];

      $result = $user->updateProfile();
      return Response::json(
        [
          'success' => true
        ]);
    }
  }