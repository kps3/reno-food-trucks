<?php
/**
 * Created by JetBrains PhpStorm.
 * User: andy
 * Date: 10/5/13
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */

abstract class BaseTruckController extends BaseController {
  public function __construct(){
    $this->beforeFilter('auth');
  }

  protected function GetProfile() {
    return Profile::one(['username' => Session::get('username')]);
  }
}