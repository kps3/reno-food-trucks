<?php
/**
 * Created by JetBrains PhpStorm.
 * User: andy
 * Date: 10/5/13
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */

abstract class BaseMobileController extends BaseController {

  private static $profile;

  public function __construct(){
    $this->beforeFilter('mobile.auth');
  }

  public static function SetProfile(Profile $profile) {
    self::$profile = $profile;
  }

  protected function GetProfile() {
    return self::$profile;
  }
}