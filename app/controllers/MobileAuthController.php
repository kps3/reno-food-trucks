<?php
  class MobileAuthController extends BaseController {
    public function Auth() {
      $input = Input::only('username', 'password');

      $query = ['username' => $input['username']];
      $profile = Profile::one($query);
      if (!$profile) {
        throw new Exception('Invalid Username or Password');
      }
      if (!Hash::check($input['password'], $profile->password)) {
        throw new Exception('Invalid Username or Password');
      }

      return Response::json(
        array(
          'success' => true,
          'token'   => $profile->getid() . '.' . md5($profile->username . $profile->password)
        )
      );

    }
  }