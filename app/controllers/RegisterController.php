<?php

  class RegisterController extends BaseController {


    public function Index() {
      return View::make('register.index')->with('foodTypes', Config::get('hardcoded.food-types'));
    }

    public function Registered() {
      return View::make('register.registered');
    }

    public function Register() {
      $input = Input::only('username', 'password', 'password2', 'tags', 'description', 'name', 'email', 'email2', 'url', 'phone');
      $user = new Profile();
      $user->username = strtolower($input['username']);
      if ($input['email'] != $input['email2']) {
        throw new Exception('Email addresses do not match.');
      }
      if ($input['password'] != $input['password2']) {
        throw new Exception('Passwords do not match.');
      }
      $user->name = $input['name'];
      $user->tags = $input['tags'];
      $user->SetPassword($input['password']);
      $user->description = $input['description'];
      $user->email = $input['email'];
      $user->social = [
        'url' => $input['url'],
        'phone' => $input['phone']
      ];
      $result = $user->register();
      return Response::json(
        [
          'success' => true
        ]);
    }

  }