<?php
  class TruckMobileController extends BaseMobileController {

    public function __construct() {
      parent::__construct();
      $this->_api = new API();
    }

    private $_api;

    public function Get() {
      $profile = $this->GetProfile();
      return Response::json($profile);
    }

    public function Update() {
      $profile = $this->GetProfile();
      extract(Input::only('lat', 'long', 'inactive'));
      if ($inactive == 1) {
        $this->_api->Update($profile->username, null, null, null, null, false, null);
      }
      else {
        $location = new Location($long, $lat);
        $this->_api->Relocate($profile->username, $location);
      }
      return Response::JSON(['success'=>true]);
    }

  }