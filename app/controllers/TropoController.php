<?php
  class TropoController extends BaseController {

    public function Text() {
      $tropo = new Tropo();
      $postdata = file_get_contents("php://input");
      $postobj = json_decode($postdata);

     // $text = 'where is goremelt'; //strtolower($postobj->session->initialText);
      $text = strtolower($postobj->session->initialText);
      //$text='where is jonathan';



      $slug = null;
      if (substr($text, 0, 8) == 'where is') {
        $slug = substr($text, 9);
      } elseif (substr($text, 0, 5) == 'where') {
        $slug = substr($text, 6);
      }
      if ($slug) {
        $api = new API();
        $result = head($api->Find(null, $slug));
        if (!$result) {
          $tropo->say('Truck not found.');
        }
        elseif ($result['active']) {
          $lat =  $result['location']['latitude'];
          $lon =  $result['location']['longitude'];
          $location = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng={$lat},{$lon}&sensor=false");
          $data = json_decode($location);
          $address = $data->results[0]->formatted_address;

          $tropo->say($result['name'] . ' is truckin\' near ' .
                      $address);
        }
        else {
          $tropo->say($result['name'] . ' is not truckin\'');
        }

      }
      else {
        $tropo->say('Slug not found.');
      }


      return $tropo->renderJSON();
    }
  }