<?php

  class HomeController extends BaseController {


    public function Index() {
      return View::make('home');
    }

    public function Trucks() {
      $api = new API();
      $filters = Input::only('location', 'tags', 'slug');
      $results = $api->Find($filters['location'], $filters['slug'], $filters['tags']);
      return Response::json($results);
    }

    public function Truck($slug) {
      $profile = Profile::one(['username' => $slug]);
      return Response::json(['image' => $profile->image, 'description' => $profile->description]);
    }
  }