<?php

  class LoginController extends BaseController {

    public function LoginPage() {
      return View::make('login');
    }

    public function Login() {
      $input = Input::only('username', 'password');

      $query =['username' => $input['username']];
      $profile = Profile::one($query);
      if (!Hash::check($input['password'], $profile->password)) {
        throw new Exception('Invalid Username or Password');
      }
      $api = new API();
      if (!$api->Find(null, $profile->username)) {
        throw new Exception('Account not activated');
      }
      Session::set('truckID', $profile->truckID);
      Session::set('username', $profile->username);
      return Redirect::route('truck');
    }


  }