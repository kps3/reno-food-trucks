<?php
  return array(
    'food-types' => [
      'sandwiches',
      'bbq',
      'pizza',
      'hotdogs',
      'burgers',
      'comfort food',
      'hawaiian',
      'asian',
      'mexican',
      'italian',
      'american',
      'sweets'
    ]
  );