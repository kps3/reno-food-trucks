<?php
  return array(

    /* Configuration section name*/
    'default' => array(
      'connection' => array(
        'hostnames' => 'localhost',
        'database'  => 'wherethefoodat',
      )
    )
  );