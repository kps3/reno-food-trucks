var LogoutView = function () {
    this.render = function () {
        app.authenticate();
        appModel.clearToken();
        window.location.hash = 'home';
    };
}
