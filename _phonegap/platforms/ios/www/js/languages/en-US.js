var language = {
    // GPS error messages
    "GPS:PERMISSION_DENIED": "Please activate GPS on your phone.",
    "GPS:POSITION_UNAVAILABLE": "Can't get your location. Is GPS turned on?",
    "GPS:TIMEOUT": "Timed out while trying to get your location.",
    "GPS:UNKNOWN_ERROR": "Uh-oh! You broke it. Please restart the app.",

    // ALERT HEADERS
    "ERROR": "Error",

    // USER errors
    "USER:AUTH_FAILED": "Sorry, that username/password combo didn't work.",

};
