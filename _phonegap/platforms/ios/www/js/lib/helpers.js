/**
 * Language Helper
 *
 */
var lang = {
    // Get language item at key
    get: function (langKey) {
        return (language[langKey] != undefined) ? language[langKey] : false;
    }
}

/**
 * Show native alert message
 */
function showAlert (alertType, message) {
    // Get title for alertType from lang
    var title = lang.get(alertType);

    if (navigator.notification) {
        navigator.notification.alert(message, null, title, 'OK');
    } else {
        alert(title ? (title + ": " + message) : message);
    }
}
