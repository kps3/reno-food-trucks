var TrackView = function () {
    this.render = function () {
        if( ! appModel.checkAuth() ) {
            window.location.hash = 'home';
            return;
        }
        this.el.html(TrackView.template());
        return this;
    };

    this.initialize = function () {
        // Define a div wrapper to attach events
        this.el = $('<div/>');
        this.el.on('click', '#toggleTracking', this.toggleTracking);
    };

    this.toggleTracking = function (event) {
        var el = $(this);
        event.preventDefault();

        console.log('toggleTracking');

        // Track Geolocation
        this.gps = new trackGPS.init();
        this.gps.toggle(appModel.pingLocation);

        var oldBtnText = el.text();
        el.text(el.data('alt-text')).data('alt-text', oldBtnText);

    };

    this.initialize();

}

TrackView.template = Handlebars.compile($("#track-view-tpl").html());
