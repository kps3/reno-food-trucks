var HomeView = function () {
    this.render = function () {
        if( appModel.checkAuth() ) {
            window.location.hash = 'track';
            return;
        }

        this.el.html(HomeView.template());
        return this;
    };

    this.initialize = function () {
        // Define a div wrapper to attach events
        this.el = $('<div class="content-wrapper" />');

        this.el.on('submit', '#login-form', this.submitLogin);
        $('body').on('authSuccess', this.loginSuccess);
    };

    this.loginSuccess = function () {
        window.location.hash = 'track';
        return;
    };

    // Log in
    this.submitLogin = function (e) {
        //e.preventDefault();
        console.log('formSubmit');
        console.log(this.checkFields);

        var username = $("input[name='uname'", this.el).removeClass('input-alert');
        var password = $("input[name='pass'", this.el).removeClass('input-alert');

        // Are fields filled out?
        if(! username.val()) {
            username.focus().addClass('input-alert');
            if(! password.val()) {
                password.addClass('input-alert');
            }
            return false;
        } else if (! password.val()) {
            password.focus().addClass('input-alert');
            return false;
        }

        appModel.auth(username.val(), password.val());
    };

    this.initialize();

}

HomeView.template = Handlebars.compile($("#home-tpl").html());
