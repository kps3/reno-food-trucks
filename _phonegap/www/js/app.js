var app = {
    // Construct
    initialize: function() {
        var self = this;

        this.routes = [
            { name: 'homePage', match: /^#home/, view: 'HomeView' }, // First route must be homepage
            { name: 'trackPage', match: /^#track/, view: 'TrackView' },
            { name: 'logoutPage', match: /^#logout/, view: 'LogoutView' },
        ];
        this.routeLength = this.routes.length;
        this.loadedRoutes = [];

        self.route();

        this.registerEvents();
    },

    // Register event bindings
    registerEvents: function() {
        $(window).on('hashchange', $.proxy(this.route, this));
    },

    // Router
    route: function () {
        var hash = window.location.hash;
        var element, match = null;

        console.log('Starting router.');

        // No hash, show home
        if (!hash) {
            if(! this.homePage) {
                console.log('router match: homepage');
                this.homePage = this.renderView(this.routes[0].view);
                this.pageTransition(this.homePage);
            }
            return;
        }
        // Check for hash routes
        for (var i = 0; i < this.routeLength; i++) {
            element = this.routes[i];
            console.log(element.match);
            match = hash.match(element.match);
            if (match) {
                console.log('router match: ' + match + '=>' + element.view);
                if(! window[element.name]) {
                    window[element.name] = this.renderView(element.view);
                }
                this.pageTransition(window[element.name]);

                return;
            }
        }

        // No routes, show home
        if(! match) {
            if(! this.homePage) {
                console.log('no matches: showing homepage');
                this.homePage = this.renderView(this.routes[0].view);
            }

            this.pageTransition(this.homePage);
        }
    },

    // Load view
    loadView: function(viewName, data) {
        var data = data || null;
        return viewObj = new window[viewName](data);
    },

    // Render view
    renderView: function(viewName, data) {
        var data = data || null;
        return this.loadView(viewName, data).render();
    },

    /* TODO */
    // Put UI page transitions inside this
    pageTransition: function(page) {
        var body = $('body');

        body.children().remove();
        body.append(page.el);
    }
};
