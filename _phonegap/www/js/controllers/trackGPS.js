/**
 * GPS Tracking Controller
 *
 */
var trackGPS = function () {
    var self,
        trackerID,
        callback,
        trackingEnabled = false,
        options = {
            timeout: 30000,
            enableHighAccuracy: true
        };

    function getInstance () {
        return {
            // Start tracking geolocation
            // returns position object to callback
            start: function (onSuccess) {
                 trackerID = navigator.geolocation.watchPosition(onSuccess, self.errorHandler, options);
                 trackingEnabled = true;

                 console.log('...geo tracking started...');
            },

            // Stop tracking geolocation
            stop: function () {
                self.clear();
                trackingEnabled = false;
                console.log('...geo tracking stopped...');
            },

            // Clear geolocation tracker
            clear: function () {
                if (trackerID != null) {
                    navigator.geolocation.clearWatch(trackerID);
                    trackerID = null;
                    trackingEnabled = false;
                }
            },

            // Reset geolocation tracker
            restart: function () {
                if (trackingEnabled) {
                    self.stop();
                    self.start();
                }
            },

            // Reset geolocation tracker
            toggle: function (onSuccess) {
                if (trackingEnabled) {
                    self.stop();
                } else {
                    self.start(onSuccess);
                }
            },

            // Error handling
            errorHandler: function (err) {
                var response = null;

                switch(err.code) {
                    case 1:
                        response = lang.get('GPS:PERMISSION_DENIED');
                        break;
                    case 2:
                        response = lang.get('GPS:POSITION_UNAVAILABLE');
                        break;
                    case 3:
                        response = lang.get('GPS:TIMEOUT');
                        break;
                    default:
                        response = lang.get('GPS:UNKNOWN_ERROR');
                        break;
                }

                showAlert('ERROR', response);
                console.log( "GPS:error => " + response );

                return response;
            },
        };
    }

    return {
        init : function () {
            if(! self) {
                self = getInstance();
            }

            return self;
        }
    };

}();
