/**
 * App Model
 *
 */
var appModel = {
    initialize: function () {
        this.apiEndpoint = 'http://wherethefoodat.com/mobile/';
        this.loggedIn = (window.localStorage.getItem("token") ) ? true : false;
    },

    auth: function (username, password) {
        var endpoint = this.apiEndpoint + 'auth';
        var data = {
            'username': username,
            'password': password
        };

        appModel.request(endpoint, data, appModel.loadToken, appModel.authError);
    },

    checkAuth: function () {
        if ( this.loggedIn ) {
            return true;
        }
        return false;
    },

    loadToken: function (jsonData) {
        if (jsonData.success) {
            showAlert('ERROR', 'it worked');
            appModel.setToken(jsonData.token);
            this.loggedIn = (window.localStorage.getItem("token") ) ? true : false;
            $("body").trigger('authSuccess');

            return true;
        }

        return false;
    },

    setToken: function (token) {
        window.localStorage.setItem("token", token);
    },

    getToken: function () {
        return window.localStorage.getItem("token");
    },

    clearToken: function () {
        window.localStorage.removeItem("token");
        this.loggedIn = false;
    },

    authError: function (xhr, error) {
        console.debug(xhr); console.debug(error);

        showAlert('ERROR', lang.get('USER:AUTH_FAILED'));
        return false;
    },

    pingLocation: function (position) {
        var endpoint = appModel.apiEndpoint + 'update';
        var data = {
            'token': appModel.getToken(),
            'lat': position.coords.latitude,
            'long': position.coords.longitude
        };

        appModel.request(endpoint, data, appModel.pingSuccess, appModel.pingError);
    },

    pingSuccess: function (data) {
        console.debug(data);
    },

    request: function (url, data, onSuccess, onError) {
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: onSuccess,
          error: onError,
          dataType: 'json'
        });
    },

}

appModel.initialize();
