var
	path = require('path'),
	flatiron = require('flatiron'),
	app = flatiron.app;

app.config.file({ file: path.join(__dirname,'config.json') });

var
	storage = require('./lib/storage'),
	routes = require('./lib/routes');

app.use(flatiron.plugins.http);
app.use(storage);
app.use(routes);

app.addStatic(__dirname + '/public', '/');
app.addController(
{
	'/status': { get: require('./lib/commands/status') },
	'/tags': { get: require('./lib/commands/tags') },
	'/find': { get: require('./lib/commands/find') },
	'/history': { get: require('./lib/commands/history') },
	'/update': { post: require('./lib/commands/update') },
});

if(process.argv[2])
	app.use(require('./' + process.argv[2]));

app.start(app.config.get('port'), function(err)
{
	if(err)
		throw err;
	
	var network = require('os').networkInterfaces();
	for(var i in network)
	{
		for(var j in network[i])
		{
			var n = network[i][j];
			if(n.family == 'IPv4' && n.internal == false)
				app.log.info("Listening on http://" + n.address + (app.config.get('port') == 80 ? '' : (':' + app.config.get('port'))) + '/');
		}
	}
});
