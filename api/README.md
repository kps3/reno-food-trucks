Where The Truck At API
======================

`Where The Truck At` has been created with a publicly accessible API. We made this API for everyone and you are free to use the data in any way that you like. We created this on our own time and with our own tools. Use of this API is completely at your own risk and we assume no responsibility for anything that happens or the integrity of the data. We tried our best to make the API is bug free, but nothing is perfect and certainly not anything that was built in 24 hours.

Status
------

The `status` command takes no input, and returns the following values:
	Title: The title of the project
	Version: The version number of the software
	Online: True/False
	Count: The total number of approved trucks

Tags
----

The `tags` command takes no input, and returns an object containing keys of the tag, and values of the number of uses of the tag.

Find
----

The `find` command can take 3 inputs: slug (to limit a single specific truck), tag (for filtering by a particular type), and location (for sorting and retuning distances).

If you specify `slug` it will only return one result, if you specify `tag` it will only return trucks which contain that tag (case sensitive), and if you specify location it will return all results it would otherwise include, but they will be sorted by distance, with the inactive trucks at the end of the list.

This command returns an array, each item of which represents a truck, and contains the following:
	Active: Currently tracking (True/False)
	Slug: Unique identifier
	Name: Full truck name
	Tags: Array of Tags
	Social: Object containing key/value pairs for things like facebook, twitter, phone, etc
	Updated: The timestamp of the last update

If the truck is active, it will also have the following:
	Location:
		Latitude:  <latitude>
		Longitude: <longitude>

If the truck is active and you supply a location, it will contain the following:
	Distance: The distance in miles between the supplied location and the truck's location.

History
-------

The `history` command takes a single parameter, `slug` (the unique identifier of the truck). The returned object contains two fields: truck and history.

The truck field will behave exactly as in the find command.

History consists of an array, each item of which contains the following:
	Timestamp: The timestamp of this update
	Location:
		Latitude:  <latitude>
		Longitude: <longitude>
