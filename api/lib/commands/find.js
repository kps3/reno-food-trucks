module.exports = function() {
	var app = this.app;
	var res = this.res;
	var filters = {};
	
	var query = this.req.query;
	
	filters.approved = true;
	
	if(query.slug) {
		filters.slug = query.slug;
	}
	
	if(query.name) {
		filters.name = query.name;
	}
	
	if(query.tag) {
		filters.tags = query.tag;
	}
	
	if(query.location) {
		query.location = query.location.split(',');
		query.location[0] = +query.location[0];
		query.location[1] = +query.location[1];
		
		filters = {
			geoNear: 'trucks',
			near: query.location,
			spherical: true,
			distanceMultiplier: 3963.1676,
			query: filters,
		};
	}
	
	if(query.location) {
		filters.query.active = true;
		app.schema.Truck.db.db.executeDbCommand(filters, function(err, response) {
			var trucks = response.documents[0].results;
			
			var response = [];
			
			for(var t in trucks) {
				var truck = {
					distance: trucks[t].dis,
					active: trucks[t].obj.active,
					slug: trucks[t].obj.slug,
					name: trucks[t].obj.name,
					tags: trucks[t].obj.tags,
					social: trucks[t].obj.social,
					updated: trucks[t].obj.updated,
					location: trucks[t].obj.loc,
				};
				
				response.push(truck);
			}
			
			filters.query.active = false;
			app.schema.Truck.find(filters.query, function(error, trucks) {
				for(var t in trucks) {
					response.push(trucks[t].normalize());
				}
				
				return res.json(200, response);
			});
		});
	}
	else {
		app.schema.Truck.find(filters, function(error, trucks) {
			var response = [];
			
			for(var t in trucks) {
				response.push(trucks[t].normalize());
			}
			
			return res.json(200, response);
		});
	}
}
