function find(slug, callback) {
	var app = this;
	app.schema.Truck.findOne({ slug: slug }, function(error, truck) {
		if(error) {
			return callback(error, null, null);
		}
		
		if(!truck) {
			return callback({ slug: 'nosuch-truck', error: 'no such `truck`' }, null, null);
		}
		
		app.schema.History.findOne({ truck: truck }, function(error, history) {
			if(error) {
				return callback(error, null, null);
			}
			
			if(!history) {
				history = new app.schema.History({ truck: truck, history: [] });
			}
			
			return callback(null, truck, history);
		});
	});
}

module.exports = function()
{
	var res = this.res;
	
	if(!this.req.query.slug) {
		return res.json(500, { slug: 'missing-slug', error: 'you must specify `slug`' });
	}
	
	find.call(this.app, this.req.query.slug, function(error, truck, his) {
		if(error) {
			return res.json(400, error);
		}
		
		his = his.history;
		var history = [];
		
		for(var i = 0; i < his.length; i++) {
			history.push({ timestamp: his[i].timestamp, location: his[i].loc });
		}
		
		return res.json(200, { truck: truck.normalize(), history: history });
	});
}
