var package = require('../../package.json');

module.exports = function()
{
	var res = this.res;
	var response =
	{
		title: package.description,
		version: package.version,
		online: true
	};
	
	this.app.schema.Truck.count({ approved: true }, function(error, count)
	{
		response.count = count;
		return res.json(200, response);
	});
}
