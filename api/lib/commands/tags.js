module.exports = function() {
	var app = this.app;
	var res = this.res;
	
	var query = this.req.query;
	
	var tags = {};
	
	app.schema.Truck.find({ approved: true }, function(error, trucks) {
		for(var t in trucks) {
			for(var i = 0; i < trucks[t].tags.length; i++) {
				var tag = trucks[t].tags[i];
				if(!tags[tag])
					tags[tag] = 0;
				
				tags[tag]++;
			}
		}
		
		var sortable = [];
		for(var t in tags) sortable.push([t, tags[t]]);
		sortable.sort(function(a, b) { return b[1] - a[1]; });
		
		tags = {};
		for(var i = 0; i < sortable.length; i++) {
			tags[sortable[i][0]] = sortable[i][1];
		}
		
		return res.json(200, tags);
	});
}
