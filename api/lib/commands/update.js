var qs = require('union/node_modules/qs');

function find(slug, callback) {
	var app = this;
	app.schema.Truck.findOne({ slug: slug }, function(error, truck) {
		if(error)
			callback(error, null, null);
		
		if(!truck) {
			truck = new app.schema.Truck({ slug: slug });
		}
		
		app.schema.History.findOne({ truck: truck }, function(error, history) {
			if(error)
				callback(error, null, null);
			
			if(!history) {
				history = new app.schema.History({ truck: truck, history: [] });
			}
			
			callback(null, truck, history);
		});
	});
}

module.exports = function()
{
	var res = this.res;
	var req = this.req;
	
	if(!req.body) {
		return res.json(500, { slug: 'missing-data', error: 'you must specify data' });
	}
	
	if(!req.body.slug) {
		return res.json(500, { slug: 'missing-slug', error: 'you must specify `slug`' });
	}
	
	find.call(this.app, req.body.slug, function(error, truck, history) {
		if(typeof req.body.approved != 'undefined') {
			truck.approved = true;
		}
		
		if(req.body.name) {
			truck.name = req.body.name;
		}
		
		if(req.body.tags) {
			truck.tags = req.body.tags.split(',');
		}
		
		if(req.body.location) {
			truck.active = true;
			
			req.body.location = req.body.location.split(',');
			req.body.location = {
				longitude: +req.body.location[0],
				latitude:  +req.body.location[1],
			};
			
			truck.loc = req.body.location;
			
			history.history.push({
				timestamp: Date.now(),
				loc: req.body.location,
			});
		}
		
		if(req.body.active == '0' || req.body.active == 'false') {
			truck.active = false;
		}
		
		if(req.body.social) {
			truck.social = qs.parse(req.body.social);
			
			for(var s in truck.social) {
				if(!truck.social[s] || !truck.social[s].length) {
					delete truck.social[s];
				}
			}
		}
		
		truck.updated = Date.now();
		
		truck.save(function(error, truck) {
			history.save(function(error, history) {
				return res.json(200, truck);
			});
		});
	});
}
