var mongoose = require('mongoose');

var schema = new mongoose.Schema(
{
	truck: { type: mongoose.Schema.Types.ObjectId, ref: 'Truck', unique: true, required: true },
	history: [{
		timestamp: { type: 'date', default: Date.now() },
		loc: {
			longitude: { type: 'Number' },
			latitude: { type: 'Number' },
		},
	}],
});

var History = module.exports = mongoose.model('History', schema);
