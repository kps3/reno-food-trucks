var mongoose = require('mongoose');

var schema = new mongoose.Schema(
{
	approved: { type: 'Boolean', default: false},
	slug: { type: 'String', required: true, unique: true },
	name: { type: 'String' },
	social: { type: 'Object', default: {} },
	tags: [{ type: 'String' }],
	updated: { type: 'Date' },
	active: { type: 'Boolean', default: false, required: true },
	loc: {
		longitude: { type: 'Number' },
		latitude: { type: 'Number' },
	},
});

schema.index({ name: 1 });
schema.index({ loc: '2dsphere' });

var Truck = module.exports = mongoose.model('Truck', schema);

Truck.prototype.normalize = function()
{
	var truck = {
		active: this.active,
		slug: this.slug,
		name: this.name,
		tags: this.tags,
		social: this.social,
		updated: this.updated,
	};
	
	if(this.active) {
		truck.location = this.loc;
	}
	
	return truck;
}
