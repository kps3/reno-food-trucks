var
	fs = require('fs'),
	mongoose = require('mongoose');

var Storage = module.exports = { };

Storage.name = 'storage';

Storage.attach = function(config)
{
	var schema = fs.readdirSync(__dirname + '/schema');
	for(var model in schema)
		require('./schema/' + schema[model]);
	
	this.schema = mongoose.models;
}

Storage.init = function(next)
{
	mongoose.connect('mongodb://nodejitsu:ca0bd0f26b166a8a43bc77fddad4c417@paulo.mongohq.com:10077/nodejitsudb6980729673');
	
	var db = mongoose.connection;
	
	db.on('error', function(error)
	{
		throw new Error(error);
	});
	
	db.once('open', function()
	{
		next();
	});
}
