var
	app = require('flatiron').app,
	st = require('st');

var Routes = module.exports = { };

Routes.name = 'routes';

Routes.attach = function(config)
{
	this.router.param('slug', /([\w-]+)/);
	this.router.before(function()
	{
		app.res = this.res;
		app.req = this.req;
		this.app = app;
		this.cookies = {};
		
		if(this.req.headers.cookie)
		{
			var pairs = this.req.headers.cookie.split(/[;,] */);
			for(var i in pairs)
			{
				var pos = pairs[i].indexOf('=');
				if(pos < 0)
					return;
				
				var key = pairs[i].substr(0, pos).trim()
				var val = pairs[i].substr(++pos, pairs[i].length).trim();
				
				if ('"' == val[0])
					val = val.slice(1, -1);
				
				this.cookies[key] = val;
			}
		}
		
		arguments[arguments.length - 1]();
	});
	
	this.addController = module.exports.addController;
	this.addStatic = module.exports.addStatic;
}

Routes.addController = function(prefix, routes)
{
	if(!routes)
	{
		routes = prefix;
		prefix = null;
	}
	
	this.router.mount(routes, prefix);
}

Routes.addStatic = function(dir,url)
{
	var options = { index: 'index.html', passthrough: true };
	options.path = dir;
	options.url = url || options.url;
	
	options.cache = false;
	
	this.http.before = this.http.before.concat(st(options));
}
