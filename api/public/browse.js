$(document).ready(function() {
	var
		form = $('#form'),
		command = $('#command'),
		output = $('#output');
	
	var input = {
		slug: $('#slug'),
		tag: $('#tag'),
		location: $('#location'),
		get: function() {
			var response = [];
			for(var i in this) {
				if(typeof this[i] != 'function' && this[i].val()) {
					response.push([i,'=',this[i].val()].join(''));
				}
			}
			
			return response.join('&');
		},
		clear: function() {
			for(var i in this) {
				if(typeof this[i] != 'function') {
					this[i].val('');
				}
			}
		},
		blur: function(callback) {
			for(var i in this) {
				if(typeof this[i] != 'function') {
					this[i].on('blur',callback);
				}
			}
		},
	};
	
	var change = function() {
		input.clear();
		
		switch(command.val()) {
			case 'status':
			case 'tags':
				input.tag.hide();
				input.slug.hide();
				input.location.hide();
			break;
			case 'find':
				input.tag.show();
				input.slug.show();
				input.location.show();
			break;
			case 'history':
				input.tag.hide();
				input.slug.show();
				input.location.hide();
			break;
		}
		
		update();
	}
	
	var update = function() {
		var url = '/' + command.val();
		
		output.html('');
		
		$.getJSON(url, input.get(), function(response, status) {
			output.html(JSON.stringify(response, null, '\t'));
		})
		.fail(function(jqXHR) {
			output.html(JSON.stringify(jqXHR.responseJSON, null, '\t'));
		});
	}
	
	input.blur(update);
	command.on('change', change).trigger('change');
	
	form.on('submit', function() { update(); return false; });
});
