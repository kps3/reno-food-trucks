var fs = require('fs');

var Load = module.exports = { };

Load.name = 'load';

var next = function() {
	next.done++;
	if(next.done == next.total)
		next.callback();
}
next.total = 0;
next.done = 0;

Load.init = function(done)
{
	next.callback = done;
	
	var trucks = require('./models/trucks');
	for(var t in trucks)
	{
		next.total++;
		trucks[t] = new this.schema.Truck(trucks[t]);
		trucks[t].save(next);
	}
}
