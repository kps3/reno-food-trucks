module.exports = 
[
	{
		slug: 'st-laurence',
		name: 'St. Laurence Pizza Co',
		tags: ['pizza'],
		social: {
			url: 'http://www.saintlawrencepizza.com/',
		},
		approved: true,
		updated: Date.now(),
		active: true,
		loc: {
			longitude: -119.8167668,
			latitude: 39.5248853,
		},
	},
	{
		slug: 'kenji',
		name: 'Kenji',
		tags: ['korean','grill'],
		approved: true,
		updated: Date.now(),
	},
	{
		slug: 'battleborn',
		name: 'Battleborne',
		tags: ['grill'],
		approved: true,
		updated: Date.now(),
	},
];
