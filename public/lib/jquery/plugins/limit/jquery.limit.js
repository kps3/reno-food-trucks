(function($){  
 $.fn.limita = function(options) {  
 
 	var defaults = {
		limit: 200,
		id_result: false,
		alertClass: false
	}
 
 	var options = $.extend(defaults, options);
    var counter = 0;
    return this.each(function() {  
    counter++;
	var	caratteri = options.limit;
    var $opt;
	if(options.id_result != false)
	{
        $opt = $("#"+options.id_result+counter);
        if (typeof($opt[0]) === 'undefined') {
            $('<div>').attr('id', options.id_result+counter).html("<strong>"+ caratteri+"</strong> characters remaining").insertAfter($(this));
            $opt = $("#"+options.id_result+counter);
        }
        else {
            $opt.append("<strong>"+ caratteri+"</strong> characters remaining");
        }

	}
	
	$(this).keyup(function(){
		if($(this).val().length > caratteri){
		$(this).val($(this).val().substr(0, caratteri));
		}
		
		if(options.id_result != false)
		{
			var restanti = caratteri - $(this).val().length;
            $opt.html("<strong>"+ restanti+"</strong> characters remaining");
		
			if(restanti <= 10)
			{
                $opt.addClass(options.alertClass);
			}
			else
			{
                $opt.removeClass(options.alertClass);
			}
		}
	});
 
});  
 };  
})(jQuery);