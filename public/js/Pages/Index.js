var IndexPage = (function ($) {

    var that = new PageBase();

    var map;

    var $trucks;

    var coords = null;
    var lastClick = null;

    var initMap = function () {
        var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(39.5248853, -119.8167668),
            mapTypeId: google.maps.MapTypeId.SATELLITE,
            panControl: true,
            panControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            }
        };
        map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
    };

    var loadTrucks = function () {
        var data = {};
        if (coords != null) {
            data.location = coords.longitude + ',' + coords.latitude;
        }
        $.get('/wherethefoodat', data).done(function (response) {
            for (var i in response) {
                var truck = response[i];
                addListItem(truck);
                if (truck.active) {
                    addMarker(truck);
                }
                else {
                    removeMarker(truck);
                }
            }
            initSort();

        }).fail(function () {

            })
    };

    var markers = {};
    var $li = {};
    var trucks = {};

    var addListItem = function (truck) {
        if (!trucks.hasOwnProperty(truck.slug)) {
            trucks[truck.slug] = true;

            var liClass = truck.active ? '' : 'class="inactive"';
            var x = !truck.active ? 'not truckin\'' : '';
            var li = '<li id="' + truck.slug + '" ' + liClass + '><a href="#">' + truck.name + '<span class="truck-location">' +x + '</span></a></li>';
            $trucks.append(li);
            $li[truck.slug] = $('#' + truck.slug);
            $li[truck.slug].data('truck', truck);
        }
    }

    var removeMarker = function (truck) {
        if (markers.hasOwnProperty(truck.slug)) {
            console.log('removing ' + truck.slug);
            markers[truck.slug].setMap(null);
            markers[truck.slug] = null;
            delete  markers[truck.slug];
            $('#trucks li#' + truck.slug).addClass('inactive');
        }
    };

    var addMarker = function (truck) {
        if (markers.hasOwnProperty(truck.slug)) {
            console.log('moving ' + truck.slug);//TODO OPtimize don't move marker if the lat/lon don't change.
            markers[truck.slug].setPosition(new google.maps.LatLng(truck.location.latitude, truck.location.longitude));
            if (truck.distance) {
                var distance = '';
                if (truck.distance > .01) {
                    distance = (Math.round(truck.distance * 10) / 10) + 'mi';
                }
                else { // feet
                    distance = (Math.round(truck.distance * 5280 * 10) / 10) + 'ft';
                }
                $li[truck.slug].removeClass('inactive');
                $li[truck.slug].find('.truck-location').text(distance);
            }
        }
        else {

            console.log('adding ' + truck.slug);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(truck.location.latitude, truck.location.longitude),
                map: map,
                //icon: '/images/icons/bob.png',
                title: truck.name,
                _id: truck.slug
            });
            google.maps.event.addListener(marker, 'click', function () {
                selectTruck(marker._id);
                getTruck(marker._id, function(response) {
                    populateTruck(marker._id, response);
                });
            });
            markers[truck.slug] = marker;
        }
    };

    var initCoords = function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                    coords = position.coords;
                    loadTrucks();
                },
                loadTrucks);
        }
        else {
            loadTrucks();
        }
    };

    function populateTruck(id, extra) {
        if ($li.hasOwnProperty(id)) {

            var truck = $li[id].data('truck');
            map.setCenter(new google.maps.LatLng(truck.location.latitude, truck.location.longitude));
            $('.truck-content .name').text(truck.name);
            if (extra.image) {
                $('.truck-content img').attr('src', extra.image);
            }
            $('.truck-content .description').html(extra.description);
            if (truck.social.phone) {
                $('.truck-content .phone').show().text(truck.social.phone);
            }
            else {
                $('.truck-content .phone').hide();
            }
            $('.truck-content .slug').text(id);
        }
    };

    function clickOnATruckDesktop() {
        var notThis = this;
        $(document).on('click', '#trucks li', function (e) {
            var id = $(this).attr('id');
            selectTruck(id);
            e.preventDefault();
            getTruck(id, function (response) {
                populateTruck(id, response);
                // set the height of the truck content
                $('.truck-content').height(window.innerHeight - 96);

                // remove classes
                $('#trucks li').removeClass('selected');
                $('.sidebar').removeClass('active');
                $('.sidebar').addClass('active');

                // wait a second to reshow and only do if it had the class active
                if ($('.truck-content').hasClass('active')) {
                    $('.truck-content').removeClass('active');
                    setTimeout(function () {
                        $('.truck-content').addClass('active');
                    }, 270);
                } else {
                    $('.truck-content').addClass('active');
                }
                $(notThis).addClass('selected');
                $("html, body").animate({ scrollTop: 0 }, "medium");
            });
        });
    }

    var getTruck = function (slug, callback) {
        $.get('/wherethefoodat/' + encodeURIComponent(slug)).done(callback)
    }

    function clickOnATruckMobile() {
        var notThis = this;
        $(document).on('click', '#trucks li', function (e) {
            var id = $(this).attr('id');
            selectTruck(id);
            e.preventDefault();
            getTruck(id, function (response) {
                populateTruck(id, response);
                // remove classes
                $('#trucks li').removeClass('selected');
                $('.sidebar').removeClass('active');
                $('.sidebar').addClass('active');
                $('.back').addClass('active');

                // wait a second to reshow and only do if it had the class active
                if ($('.truck-content').hasClass('active')) {
                    $('.truck-content').removeClass('active');
                    setTimeout(function () {
                        $('.truck-content').addClass('active');
                    }, 270);
                } else {
                    $('.truck-content').addClass('active');
                }
                $(notThis).addClass('selected');
                $("html, body").animate({ scrollTop: 0 }, "medium");

                var heightOfContent = $('.truck-content.active .content').height();
                $('.container').height(heightOfContent + window.innerHeight - 64);
            });
        });

    }

    var selectTruck = function (id) {
        if (id == lastClick) return;
        if (lastClick && markers.hasOwnProperty(lastClick)) {
            markers[lastClick].setAnimation(null);
        }
        if (markers.hasOwnProperty(id)) {
            markers[id].setAnimation(google.maps.Animation.BOUNCE);
        }
        lastClick = id;
    }

    that.Init = function () {
        $trucks = $('#trucks');
        initCoords();
        initMap();
        loadTrucks();
        if (isMobile.any()) {
            clickOnATruckMobile();
        } else {
            clickOnATruckDesktop();
        }
        setInterval(loadTrucks, 5000);
    };

    var initSort = function() {
        return;
        var $Ul = $('ul#trucks');
        $Ul.css({position:'relative',height:$Ul.height(),display:'block'});
        var iLnH;
        var $Li = $('ul#trucks>li');
        $Li.each(function(i,el){
            var iY = $(el).position().top;
            $.data(el,'h',iY);
            if (i===1) iLnH = iY;
        });
        $Li.tsort('span.truck-location').each(function(i,el){
            var $El = $(el);
            var iFr = $.data(el,'h');
            var iTo = i*iLnH;
            $El.css({position:'absolute',top:iFr}).animate({top:iTo},500);
        });
    };

})
    (jQuery);