var TruckPage = (function ($) {

    var that = new PageBase();
    var coords = null;
    var locateID = null;
    var onControlButton = function () {
        $(this).data('control') === 'start' ? onStart() : onStop();
    };

    var onStart = function () {
        if (!locateID) {
            startLocating();
        }
    };

    var onStop = function () {
        console.log('stopping');

        if (navigator.geolocation) {
            navigator.geolocation.clearWatch(locateID);
        }

        doButton({start: false});
    };

    var doButton = function (data) {
        $.post('/truck/button', data).done(function (response) {
            $status = $('#status');
            if (data.start) {
                $status.text('Active');
            }
            else {
                $status.text('Inactive');
                that.DisplaySuccess('Stopped');
            }
        }).fail(function () {
                that.DisplayError('Could not execute');
            });
    };

    var storePosition = function (position) {
        coords = position.coords;
        var data = {start: true,
            lat: coords.latitude,
            long: coords.longitude
        };
        doButton(data);
    };

    var errorPosition = function () {
        navigator.geolocation.clearWatch(locateID);
        locateID = navigator.geolocation.watchPosition(storePosition);
    };

    var startLocating = function () {
        if (navigator.geolocation) {
            console.log('start to locate');
            navigator.geolocation.clearWatch(locateID);
            locateID = navigator.geolocation.watchPosition(storePosition, errorPosition, { enableHighAccuracy: true });
        }
    };

    var initPosition = function () {
        $('.show-on-location').show();
    }


    that.Init = function () {
        $('.control-button').click(onControlButton);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(initPosition);
            ;
        }
        else {
            that.DisplayError('Location services are not available from your device, or your location could not be determined.');
        }
    };

})(jQuery);