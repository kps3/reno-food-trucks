var Register = (function () {

    that = new PageBase();

    var onSubmit = function (e) {
        e.preventDefault();
        that.ResetMessages();
        var $button = $('#button');
        var $target = $('#spinner');
        $button.hide();
        $target.show();
        var spinner = new Spinner().spin($target[0]);
        var data = $('#truck-profile').serialize();
        $.post('/truck/profile', data).done(
            function (response) {
                if (response.success) {
                    that.DisplaySuccess('Profile Successfully Updated');
                }
                else {
                    that.DisplayError('Porfile Could Not Be Updated : '+ response.error.message);
                }
                $button.show();
                $target.hide();
            }
        ).
            fail(function () {
                that.DisplaySuccess('Profile Could Not Be Updated');
                $button.show();
                $target.hide();
            }
        );
        return false;
    };

    that.Init = function () {
        $('#truck-profile').on('submit', onSubmit);
    };

})(jQuery);