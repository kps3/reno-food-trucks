var Register = (function () {

    that = new PageBase();

    var onSubmit = function (e) {
        e.preventDefault();
        that.ResetMessages();
        var $button = $('#button');
        var $target = $('#spinner');
        $button.hide();
        $target.show();
        var spinner =  new Spinner().spin($target[0]);
        var data = $('#register-form').serialize();
        $.post('/register', data).done(function (response) {
                console.log(response);
                if (response.success) {
                    that.BrowseTo('/registered');
                }
                else {
                    that.DisplayError('Unable to register :'  + response.error.message);
                    $button.show();
                    $target.hide();
                }
            }
        ).fail(
            function () {
                that.DisplayError('Unable to register');
                $button.show();
                $target.hide();
            }
        );
        return false;
    };

    that.Init = function () {
        $('#register-form').on('submit', onSubmit);
    };

})(jQuery);