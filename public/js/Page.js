var PageBase = function () {
    "use strict";

    var self = {};

    self.Dirty = false;

    self.Init = function () {

    };

    /**
     * @param url
     * @returns {boolean}
     */
    self.BrowseTo = function (url) {
            document.location.href = url;
        return false;
    };

    self.SetDirty = function () {
        self.Dirty = true;
    };

    self.DisplayError = function (message, id) {
        id = id ? id : '#message-error';
        $(id).text(message).slideDown(200, function () {
            $(this).removeClass('hidden');
        });
    };

    self.DisplaySuccess = function (message, id) {
        id = id ? id : '#message-success';
        $(id).text(message).slideDown(200, function () {
            $(this).removeClass('hidden');
        });
    };

    self.ResetMessages = function (prefix) {
        var messages = ['success', 'error'];
        prefix = prefix ? prefix : '#message-';
        for (var i in messages) {
            $(prefix + messages[i]).text('').slideUp(200, function () {
                $(this).addClass('hidden');
            });
        }
    };

    /**
     * @returns {null|string}
     * @constructor
     */
    self.BeforeUnload = function (e) {
        var message = null;
        if (self.Dirty) {
            message = 'There are unsaved changes on this page.  Are you sure you want to leave this page without saving?';
            e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
        } // only show if there is dirty data.
        return message;
    };

    jQuery(document).ready(function () {
        self.Init();
        if (!window.onbeforeunload) { // Do not override if it is already set
            window.onbeforeunload = self.BeforeUnload;
        }
    });

    return self;
};
