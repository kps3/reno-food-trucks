var isMobile;
$(document).ready(function() {

     isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    if( isMobile.any() ) {
        mobileWidthsAndHeights();
        closeMe();
        menuClick();
    } else {
        desktopWidthsAndHeights();
        closeMe();
        menuClick();
    }

    function desktopWidthsAndHeights() {
        var height = window.innerHeight;
        $('.map').height(height - 96);
        $('.sidebar').height(height - 96);
        $('#trucks').height(height - 142);
        // funky nav height
        $('#nav').height(height - 70 );
    }

    function mobileWidthsAndHeights() {
        var windowHeight = window.height;
        var documentHeight = document.height;
        var width = document.width;
        $('.map').height(documentHeight - 120);
        $('.header,.container').width(width).removeClass('animated');
        $('.container').height(windowHeight).css("overflow","hidden");
    }

    function closeMe() {
        $('.close,.back').click(function(e) {
            e.preventDefault();
            $('#trucks li').removeClass('selected');
            $('.back').removeClass('active');
            $('.truck-content').removeClass('active');
            $('.sidebar').removeClass('active');
            $("html, body").animate({ scrollTop: 0 }, "medium");
        });
    }

    function menuClick() {
        $('#menu,#about').click(function() {
            $('.container').toggleClass('active');
            $('#menu').toggleClass('active');
        });
    }

    setTimeout(function() {
        $('.copyright').addClass('active');
    }, 500);

});
