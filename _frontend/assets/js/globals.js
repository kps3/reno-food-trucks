$(document).ready(function() {

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    if( isMobile.any() ) {
        mobileWidthsAndHeights();
        closeMe();
        clickOnATruckMobile();
        menuClick();
    } else {
        desktopWidthsAndHeights();
        closeMe();
        clickOnATruckDesktop();
        menuClick();
    }

    function desktopWidthsAndHeights() {
        var height = window.innerHeight
        $('.map').height(height - 96);
        $('.sidebar').height(height - 96);
        $('#trucks').height(height - 142);
        // funky nav height
        $('#nav').height(height - 70 );
    }

    function mobileWidthsAndHeights() {
        var windowHeight = window.height;
        var documentHeight = document.height;
        var width = document.width;
        $('.map').height(documentHeight - 120);
        $('.header,.container').width(width).removeClass('animated');
        $('.container').height(windowHeight).css("overflow","hidden");
    }

    function closeMe() {
        $('.close,.back').click(function(e) {
            e.preventDefault();
            $('#trucks li').removeClass('selected');
            $('.back').removeClass('active');
            $('.truck-content').removeClass('active');
            $('.sidebar').removeClass('active');
            $("html, body").animate({ scrollTop: 0 }, "medium");
        });
    }

    function clickOnATruckDesktop() {
        $('#trucks li:not(".inactive")').click(function(e) {
            // set the height of the truck content
            $('.truck-content').height(window.innerHeight - 96);

            // remove classes
            $('#trucks li').removeClass('selected');
            $('.sidebar').removeClass('active');
            $('.sidebar').addClass('active');

            // wait a second to reshow and only do if it had the class active
            if ( $('.truck-content').hasClass('active') ) {
                $('.truck-content').removeClass('active');
                setTimeout(function() {
                    $('.truck-content').addClass('active');
                }, 270);
            } else {
                $('.truck-content').addClass('active');
            }
            $(this).addClass('selected');
            $("html, body").animate({ scrollTop: 0 }, "medium");
            e.preventDefault();
        });
    }

    function clickOnATruckMobile() {
        $('#trucks li:not(".inactive")').click(function(e) {
            e.preventDefault();
            // remove classes
            $('#trucks li').removeClass('selected');
            $('.sidebar').removeClass('active');
            $('.sidebar').addClass('active');
            $('.back').addClass('active');

            // wait a second to reshow and only do if it had the class active
            if ( $('.truck-content').hasClass('active') ) {
                $('.truck-content').removeClass('active');
                setTimeout(function() {
                    $('.truck-content').addClass('active');
                }, 270);
            } else {
                $('.truck-content').addClass('active');
            }
            $(this).addClass('selected');
            $("html, body").animate({ scrollTop: 0 }, "medium");

            var heightOfContent = $('.truck-content.active .content').height();
            $('.container').height(heightOfContent + window.innerHeight - 64);

        });

    }

    function menuClick() {
        $('#menu,#about').click(function() {
            $('.container').toggleClass('active');
            $('#menu').toggleClass('active');
        });
    }

    setTimeout(function() {
        $('.copyright').addClass('active');
    }, 500);

});
